/// <reference types="cypress" />

import Home_Page   from '../PageObjects/Home_Page'
import Login_Page from '../PageObjects/Login_Page' 
import BackgroundInfo_Page from '../PageObjects/BackgroundInfo_Page'
import UploadFile_Page from '../PageObjects/UploadFiles'
import PostJob_Page from '../PageObjects/PostJob_Page'

describe('PostJob TestSuit', function()  {

    before(function()
    {
        //load the Data from Json File
        cy.fixture('UserData').then(function(data){
          this.data = data
        })
        //load the Data from Json File
        cy.fixture('JobData').then(function(data1){
          this.data1 = data1
        })
        //Open the Url
        cy.visit('https://staging.vodafonebegin.com') // Open Url
    
      })
  
    
    it('Post a Job', function() {
        const HomePageObject = new Home_Page()
        HomePageObject.Open_Login_Page()
          
        
        const Login_PageObject = new Login_Page()
        Login_PageObject.fillElements(this.data.Email,this.data.Password)
        
        const BackgroundInfo_PageObject = new BackgroundInfo_Page()
       
        BackgroundInfo_PageObject.fillElements
          (this.data1.Job_title,this.data1.Job_Description,this.data1.Job_minBudget,
          this.data1.Job_maxBudget,this.data1.Job_Skill,this.data1.Job_Category,this.data1.Job_Date)
      

        const UploadFile_PageObject = new UploadFile_Page()
        UploadFile_PageObject.uploadFile()

        const PostJob_PageObject = new PostJob_Page()
        PostJob_PageObject.PostJob()

      })


      after(function()
      {
        cy.log("Test Suit is Done !!!!")
      })
    })