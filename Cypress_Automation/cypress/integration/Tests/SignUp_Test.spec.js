/// <reference types="cypress" />

import Home_Page   from '../PageObjects/Home_Page'
import SignUp_Page from '../PageObjects/SignUp_Page' 
import BackgroundInfo from '../PageObjects/BackgroundInfo_Page'
describe('Registration TestSuit', function()  {


  

    before(function()
    {
        //Open the Url
       cy.visit('https://staging.vodafonebegin.com') // Open Url
    
      })
  
    beforeEach(function(){
       //load the Data from Json File
       cy.fixture('SignUpData').then(function(data){
        this.data = data
      })
      const HomePageObject = new Home_Page()
      HomePageObject.Open_SignUp_Page()
    

    })
      it('Valid Registration ', function() {
    
        const SignUp_PageObject = new SignUp_Page()
        SignUp_PageObject.FillElements
        (this.data[0].FirstName,this.data[0].LastName,this.data[0].Email,this.data[0].Password,this.data[0].Conf_Pass)

        // return from the background info page
        const BackgroundInfoObject = new BackgroundInfo()
        BackgroundInfoObject.ClientLogOut()
        cy.wait(500)
      })
      it('Invalid Registration Email is already Exist  ', function() {
       
        const SignUp_PageObject = new SignUp_Page()
        SignUp_PageObject.FillElements
        (this.data[1].FirstName,this.data[1].LastName,this.data[1].Email,this.data[1].Password,this.data[1].Conf_Pass)
        cy.wait(3000)
        cy.get('.ant-notification-notice-message').should('contain','Email has already been taken')
 
      })
      it('Invalid Registration Email incorrect format  ', function() {
        
        cy.wait(500)
        cy.get("#firstName").scrollIntoView().should('be.visible')
        cy.wait(500)
        const SignUp_PageObject = new SignUp_Page()
        SignUp_PageObject.FillElements
        (this.data[2].FirstName,this.data[2].LastName,this.data[2].Email,this.data[2].Password,this.data[2].Conf_Pass)
        cy.wait(1500)
        cy.get('.ant-form-explain').should('contain','Please enter a valid email address')
        
  
      })

      after('Test Suit is Done !!!!',function(){})
    })